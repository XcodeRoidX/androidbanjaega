package com.design.hrh.banjaegaandroidapp.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by MuhammadAbubakar on 3/4/2018.
 */

public class SocialUserModel  {

    private String userName = "";
    private String userType = "";
    private String userID ="";
    private String token = "";
    private URL imageUrl = null;
    private String userEmail = "";

    public SocialUserModel(JSONObject object,String type) {
        try {
            URL img_value = null;
            if(type.matches("FB")){
                setUserType("Facebok");
                try {
                    img_value = new URL("https://graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else if (type.matches("Google")){
                try {
                    img_value =  new URL(object.getString("imageUrl"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                setImageUrl(img_value);
                setUserType("Google");
            }
            setUserID(object.getString("id"));

            setImageUrl(img_value);
            setUserName(object.getString("name"));
            if(object.has("email"))
                setUserEmail(object.getString("email"));
            else
                setUserEmail("noemail@gmail.com");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public URL getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(URL imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }



    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
