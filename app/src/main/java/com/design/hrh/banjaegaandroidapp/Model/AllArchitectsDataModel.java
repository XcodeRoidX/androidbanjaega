package com.design.hrh.banjaegaandroidapp.Model;

import android.content.Context;

import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;

import org.json.JSONObject;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class AllArchitectsDataModel {
    private String archId = "";
    private String Id = "";
    private String userName = "";
    private String phoneNumer = "";
    private String logo = "";
    private String archCity = "";
    private String archCountry = "";
    private String archaddress = "";
    private String latitude = "";
    private String longitude = "";
    private String views = "";
    private String flags = "";


    public AllArchitectsDataModel(Context context, JSONObject jsonObject) {
        try {
            setArchId(MyHelper.checkStringIsNull(jsonObject, "user_id", ""));
            setUserName(MyHelper.checkStringIsNull(jsonObject, "username", ""));
            setPhoneNumer(MyHelper.checkStringIsNull(jsonObject, "phone", ""));
            setArchaddress(MyHelper.checkStringIsNull(jsonObject, "address", ""));
            setLogo(MyHelper.checkStringIsNull(jsonObject, "logo", ""));
            setArchCountry(MyHelper.checkStringIsNull(jsonObject, "country", ""));
            setArchCity(MyHelper.checkStringIsNull(jsonObject, "city", ""));
            setLatitude(MyHelper.checkStringIsNull(jsonObject, "lat", ""));
            setLongitude(MyHelper.checkStringIsNull(jsonObject, "long", ""));
            setViews(MyHelper.checkStringIsNull(jsonObject, "views", ""));
            setFlags(MyHelper.checkStringIsNull(jsonObject, "user_type", ""));
            setId(MyHelper.checkStringIsNull(jsonObject, "id", ""));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getArchId() {
        return archId;
    }

    public void setArchId(String archId) {
        this.archId = archId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumer() {
        return phoneNumer;
    }

    public void setPhoneNumer(String phoneNumer) {
        this.phoneNumer = phoneNumer;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getArchCity() {
        return archCity;
    }

    public void setArchCity(String archCity) {
        this.archCity = archCity;
    }

    public String getArchCountry() {
        return archCountry;
    }

    public void setArchCountry(String archCountry) {
        this.archCountry = archCountry;
    }

    public String getArchaddress() {
        return archaddress;
    }

    public void setArchaddress(String archaddress) {
        this.archaddress = archaddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

}
