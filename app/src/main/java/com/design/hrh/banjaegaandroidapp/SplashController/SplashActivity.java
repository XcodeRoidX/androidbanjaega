package com.design.hrh.banjaegaandroidapp.SplashController;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.util.SparseLongArray;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.design.hrh.banjaegaandroidapp.AppController.MapsParent;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.design.hrh.banjaegaandroidapp.Utills.RuntimePermissionsActivity;
import com.google.maps.android.clustering.ClusterManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class SplashActivity extends RuntimePermissionsActivity {
    final private int REQUEST_PERMISSIONS = 20;
    int duration = 10000; // milliseconds
    public static String activityName;
    private static final String TAG = MapsParent.class.getSimpleName();

    double latitude = 33.7205829, longitude = 73.0736015;

    public static String getActivityName() {
        return activityName;
    }

    public static void setActivityName(String activity) {
        activityName = activity;
    }

    private ClusterManager<MapsParent.StringClusterItem> mClusterManager;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    DataHelper dataHelper;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        gps=new GPSTracker(this);
        if (gps.canGetLocation()) {
            gps = new GPSTracker(this);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            latitude = 33.7205829;
            longitude = 73.0736015;
            gps.showSettingsAlert();
        }
        initVolleyCallback();

        dataHelper = DataHelper.getInstance();
        mVolleyService = new VolleyService(mResultCallback, this);
        if(latitude == 0.0)
            latitude = 33.7205829;         longitude = 73.0736015;


        mVolleyService.getDataVolley("GETCALL", "search");
        JSONObject sendObj = null;

        SplashActivity.super.requestAppPermissions(new
                        String[]{
                        android.Manifest.permission.INTERNET,
                        android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.BLUETOOTH,
                        android.Manifest.permission.ACCESS_NOTIFICATION_POLICY,
                        android.Manifest.permission.SET_ALARM,
                }, R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MapsParent.class));
                    finish();
                }
            }, duration);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dataHelper.arrayArchitecht != null) {
                    startActivity(new Intent(SplashActivity.this, MapsParent.class));
                    finish();

                }
            }
        }, duration);
    }


    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Progressloader.hideloader();
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {

                onResponseFinished(response);
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
//                Progressloader.hideloader();
              //  startActivity(new Intent(SplashActivity.this,MapsParent.class));
                if (error instanceof NetworkError) {

                    Toast.makeText(SplashActivity.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();

                    //TODO
                }
                else if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(SplashActivity.this,
                            "Please check your internet connection and try again",
                            Toast.LENGTH_LONG).show();
                }

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");
            }

            @Override
            public void notifySucessEmail(String requestType, String response) {

            }

            @Override
            public void notifyLocationsucess(String requestType, JSONObject response) {

            }

            @Override
            public void notifyRatingSuccess(String requestType, JSONArray response) {

            }
        };
    }

    private void onResponseFinished(JSONArray response) {
        try {
            if (dataHelper.arrayArchitecht == null) {
                dataHelper.arrayArchitecht = new ArrayList<>();
            }
            if (response.length() > 0) {
                dataHelper.arrayArchitecht.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    AllArchitectsDataModel dataModel = new AllArchitectsDataModel(SplashActivity.this, jsonObject);
                    dataHelper.arrayArchitecht.add(dataModel);
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

}
