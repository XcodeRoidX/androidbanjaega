package com.design.hrh.banjaegaandroidapp.Model;

/**
 * Created by Haroon G on 3/1/2018.
 */

public class NavScrollitems {

    int drawable;
    int changedrawable;
    String imagetag;
    boolean isImagechanged;

    public boolean isImagechanged() {
        return isImagechanged;
    }

    public void setImagechanged(boolean imagechanged) {
        isImagechanged = imagechanged;
    }


    public int getChangedrawable() {
        return changedrawable;
    }

    public void setChangedrawable(int changedrawable) {
        this.changedrawable = changedrawable;
    }


    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }


    public String getImagetag() {
        return imagetag;
    }

    public void setImagetag(String imagetag) {
        this.imagetag = imagetag;
    }


}
