package com.design.hrh.banjaegaandroidapp.ExternalFiles.Request;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IResult {
    public void notifySuccess(String requestType, JSONObject response);

    public void notifySuccess(String requestType, JSONArray response);

    public void notifyError(String requestType, VolleyError error);

    public void notifySucessEmail(String requestType, String response);

    public void notifyRatingSuccess(String requestType, JSONArray response);

    void notifyLocationsucess(String requestType, JSONObject response);
}
