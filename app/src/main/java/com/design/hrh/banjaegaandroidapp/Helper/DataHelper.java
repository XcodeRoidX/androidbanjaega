package com.design.hrh.banjaegaandroidapp.Helper;

import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;

import java.util.ArrayList;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class DataHelper {

    public static DataHelper instance;
    public ArrayList<AllArchitectsDataModel> arrayArchitecht;

    public DataHelper(){

    }

    public static DataHelper getInstance (){
        if (instance == null)
            instance = new DataHelper();
        return instance;
    }

}
