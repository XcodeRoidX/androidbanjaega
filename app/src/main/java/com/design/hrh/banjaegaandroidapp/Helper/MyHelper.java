package com.design.hrh.banjaegaandroidapp.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.design.hrh.banjaegaandroidapp.Model.SocialUserModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class MyHelper {
    private static MyHelper instance = null;
    public static Context context;
    public ArrayList<SocialUserModel> socialUserModels = new ArrayList<>();

    public MyHelper(Context mContext)
    {
        context = mContext;

    }

    public static MyHelper getInstance(Context mContext) {
        if (instance == null) {
            instance = new MyHelper(mContext);
            SharedPreferences prefJobDetail = mContext.getSharedPreferences("jobDetailData", Context.MODE_PRIVATE);
            prefJobDetail.edit().putBoolean("isApiFirstCall",false).apply();
        }
        context = mContext;
        return instance;
    }
    public static String checkStringIsNull(JSONObject jsonObject, String parameter){
        String value = "";
        try{
            if (jsonObject.has(parameter)){
                if (!jsonObject.isNull(parameter) && !jsonObject.getString(parameter).equals("[]"))
                    value = jsonObject.getString(parameter);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return value;
    }

    public static String checkStringIsNull(JSONObject jsonObject, String parameter,String defaultValue) {
        String value = defaultValue;
        try{
            if (jsonObject.has(parameter)){
                if (!jsonObject.isNull(parameter) && !jsonObject.getString(parameter).equals("[]"))
                    value = jsonObject.getString(parameter);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return value;
    }

}
