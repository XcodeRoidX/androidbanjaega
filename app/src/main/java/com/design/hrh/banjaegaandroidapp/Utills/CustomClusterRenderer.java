package com.design.hrh.banjaegaandroidapp.Utills;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;

import com.design.hrh.banjaegaandroidapp.AppController.MapsParent;
import com.design.hrh.banjaegaandroidapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

/**
 * Created by MuhammadAbubakar on 2/25/2018.
 */

public class CustomClusterRenderer extends DefaultClusterRenderer<MapsParent.StringClusterItem> {

    private final Context mContext;
    private final IconGenerator mClusterIconGenerator;

    public CustomClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<MapsParent.StringClusterItem> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        mClusterIconGenerator = new IconGenerator(mContext.getApplicationContext());

    }

    @Override protected void onBeforeClusterItemRendered(MapsParent.StringClusterItem item, MarkerOptions markerOptions) {
        final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
        markerOptions.icon(markerDescriptor).snippet(item.title);




    }

    @Override protected void onBeforeClusterRendered(Cluster<MapsParent.StringClusterItem> cluster,
                                                     MarkerOptions markerOptions) {
        mClusterIconGenerator.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_circle));
        mClusterIconGenerator.setTextAppearance(R.style.CustomTextTheme);
        final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));


       /* List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
        int width = mDimension;
        int height = mDimension;

        for (Person p : cluster.getItems()) {
            // Draw 4 at most.
            if (profilePhotos.size() == 4) break;
            Drawable drawable = getResources().getDrawable(p.profilePhoto);
            drawable.setBounds(0, 0, width, height);
            profilePhotos.add(drawable);
        }
        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
        multiDrawable.setBounds(0, 0, width, height);

        mClusterImageView.setImageDrawable(multiDrawable);
        Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));*/






    }
    /*@Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        return cluster.getSize() > 5; // if markers <=5 then not clustering
    }*/
}
