package com.design.hrh.banjaegaandroidapp.AppController;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.design.hrh.banjaegaandroidapp.Adapters.Adapters_Nav;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.DataHelper;
import com.design.hrh.banjaegaandroidapp.Helper.Progressloader;
import com.design.hrh.banjaegaandroidapp.Model.AllArchitectsDataModel;
import com.design.hrh.banjaegaandroidapp.Model.NavScrollitems;
import com.design.hrh.banjaegaandroidapp.R;
import com.design.hrh.banjaegaandroidapp.Utills.CustomClusterRenderer;
import com.design.hrh.banjaegaandroidapp.Utills.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class MapsParent extends FragmentActivity implements OnMapReadyCallback, LocationSource.OnLocationChangedListener, View.OnClickListener {
    private static LatLng LOCATIONS_POINTS = null;
    private GoogleMap mMap;
    private ClusterManager<StringClusterItem> mClusterManager;
    DataHelper dataHelper;
    private boolean isFoundArch = false;
    CardView contactInfo, cardEmailCall, cardupdateloc;
    RelativeLayout cardProfileInfo;
    TextView title, tvHeading, tvAddressName, tvOptionDesc, btnRateMe;
    ImageView canceCard, cancelArchView, cancelupdatefrom;
    Button buttonSend, buttonSendVendorloc;
    EditText etUsername, etContact, etVendorName, etVendorContact, etvendorLocation;
    ImageView ivThumbs, ivHome, ivCurtain, ivManGrey, ivFurniture, ivBath, ivNearBy, ivKitchen, ivCall, ivMsg, ivAdminCall, ivAdminMessage,
            ivCardHome, ivCardInterior, ivCardArch, ivCardCal, ivNext;

    double currentLatitude, currentLongitude;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    GPSTracker gpsTracker;
    private static final String TAG = MapsParent.class.getSimpleName();
    int viewWidth, mWidth, PLACE_AUTOCOMPLETE_REQUEST_CODE = 1, PLACE_AUTOCOMPLETE_REQUEST_CODE1 = 2;
    String ImgTag = "No", Usertype = "", phoneNum, bitmapString, Showhidekeyboard = "yes";

    LinearLayout footerLayout;
    RadioButton emailView, archProfileView, updateloc;

    AutoCompleteTextView autoCompleteTextView, autoCompleteTextViewLoc;

    double latitude = 33.7205829, longitude = 73.0736015, searchLatitude, searchLongitude, vendorLatitude, vendorLongitude;
    CircleImageView circleImageView;
    String EMAILTAG;


    Intent archtectactivityintent;

    RecyclerView recyclerView;
    Adapters_Nav adapters_navicons;

    private ArrayList<NavScrollitems> arraynavicons = new ArrayList<>();
    private ArrayList<String> autocompletearray = new ArrayList<>();
    private NavScrollitems Navscrollitemes;
    RadioGroup radioGroup;


    // Top menu default images,change on click images and tag arrays

    int[] myImageList = new int[]{R.drawable.man_grey, R.drawable.img_kitchen, R.drawable.furniture_grey,
            R.drawable.bath_grey, R.drawable.tiles_grey, R.drawable.home_grey, R.drawable.lights_grey};
    int[] myImageListcolor = new int[]{R.drawable.man_yellow, R.drawable.kitchen_yellow, R.drawable.furniture_yellow,
            R.drawable.bath_yellow, R.drawable.tiles_yellow, R.drawable.home_yellow, R.drawable.lighting_yellow};
    String[] imagestag = new String[]{"home", "kitchen", "furniture", "bath", "tiles", "interior", "lights"};


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_layout);
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            gpsTracker = new GPSTracker(this);
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            searchLatitude = gpsTracker.getLatitude();
            searchLongitude = gpsTracker.getLongitude();

        } else {
            gpsTracker.showSettingsAlert();
        }
        initalizeControls();
        initVolleyCallback();

        //  Recyclervha
        // iew  top menu
        populaterecyclerviewdata();


        dataHelper = DataHelper.getInstance();
        if (savedInstanceState == null) {
            setupMapFragment();
        }

        onClickCalls();

        changelatlongtoaddress(latitude, longitude);

        contactInfo.setVisibility(View.GONE);
        mVolleyService = new VolleyService(mResultCallback, this);
        JSONObject sendObj = null;
        Display display = getWindowManager().getDefaultDisplay();
        mWidth = display.getWidth(); // deprecated
        viewWidth = mWidth / 3;

    }

    private void onClickCalls() {
        canceCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true);  // default selection of radio button ist one
                contactInfo.setVisibility(View.GONE);
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);

                //setting  default values of email and updatelocation card
                clearcardstodefault();
            }
        });

        cancelArchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                clearcardstodefault();
            }
        });

        cancelupdatefrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailView.setChecked(true); // default selection of radio button ist one
                cardProfileInfo.setVisibility(View.GONE);
                contactInfo.setVisibility(View.GONE);
                cardupdateloc.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                clearcardstodefault();
            }
        });

        ivAdminCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall("03041116663");
            }
        });


        ivAdminMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS("03000801546");
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (etContact.getText().length() == 0 || etUsername.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter your information", Toast.LENGTH_SHORT).show();
                } else {
                    ivThumbs.setVisibility(View.VISIBLE);
                    contactInfo.setVisibility(View.GONE);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.get_sendemailDataVolley("GETCALL", "sendmail&name=" + etUsername.getText().toString() + "&contact=" + etContact.getText().toString() + "&type=" + EMAILTAG);

                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataHelper.arrayArchitecht != null) {
                            ivThumbs.setVisibility(View.GONE);
                        }
                    }
                }, 500);
            }
        });

        emailView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.VISIBLE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

        updateloc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.GONE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        archProfileView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (b) {
                        cardProfileInfo.setVisibility(View.GONE);
                        contactInfo.setVisibility(View.VISIBLE);
                        cardEmailCall.setVisibility(View.GONE);
                        cardupdateloc.setVisibility(View.GONE);
                    }
                }
            }
        });

        btnRateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSocialLogin();
            }
        });

        buttonSendVendorloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etVendorName.getText().length() == 0 || etVendorContact.getText().length() == 0 || etvendorLocation.getText().length() == 0) {
                    Toast.makeText(MapsParent.this, "Please enter complete information", Toast.LENGTH_SHORT).show();
                } else if (emailValidator(etVendorContact.getText().toString())) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.updateUserLocation("GetCall", "https://www.banjaiga.com/web_services/index.php/updateLocation/"
                            + tvHeading.getText().toString() + "/" + Usertype + "/" + etVendorName.getText().toString() + "/"
                            + etVendorContact.getText().toString() + "/" + vendorLatitude + "/" + vendorLongitude
                    );
                } else {

                    Toast.makeText(MapsParent.this, "Please enter valid email", Toast.LENGTH_SHORT).show();

                }

            }
        });

        etvendorLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)//.setFilter(typeFilter)
                            .build(MapsParent.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE1);
                } catch (GooglePlayServicesRepairableException e) {
                    isGooglePlayServicesAvailable(MapsParent.this);
                } catch (GooglePlayServicesNotAvailableException e) {
                    isGooglePlayServicesAvailable(MapsParent.this);
                }
            }
        });

        autoCompleteTextViewLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showplacefragment(v);
            }


        });


        autoCompleteTextViewLoc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search&search=" + autoCompleteTextView.getText().toString() + "&lat=" + searchLatitude + "&long=" + searchLongitude);
                    ClearSearchview(v);
                    return true;
                }
                return false;
            }
        });



        /*autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search&search=" + autoCompleteTextView.getText().toString() + "&lat=" + "" + "&long=" + "");
                   ClearSearchview();
                    return true;
                }
                return false;
            }
        });
*/

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                Progressloader.loader(MapsParent.this);
                String query = autoCompleteTextView.getText().toString().replace(" ", "%20");
                mVolleyService.getDataVolley("GETCALL", "search&search=" + query + "&lat=" + "" + "&long=" + "");
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(arg1.getApplicationWindowToken(), 0);

            }

        });

        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < 1) {
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void clearcardstodefault() {
        ivCardHome.setImageResource(R.drawable.projects_round);
        ivCardInterior.setImageResource(R.drawable.vendor_round);
        ivCardArch.setImageResource(R.drawable.man_greyround);
        ivCardCal.setImageResource(R.drawable.calculator_round);
        etContact.setText("");
        etUsername.setText("");
        etVendorContact.setText("");
        etVendorName.setText("");
        EMAILTAG = "";

    }

    private void showSocialLogin() {
        startActivity(archtectactivityintent);

    }


    public void showplacefragment(View view) {
        try {
            //AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)//.setFilter(typeFilter)
                    .build(MapsParent.this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            isGooglePlayServicesAvailable(MapsParent.this);
        } catch (GooglePlayServicesNotAvailableException e) {
            isGooglePlayServicesAvailable(MapsParent.this);
        }

    }


    public boolean emailValidator(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean phonevalidator(String phone) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^((\\+92)|(0092))-{0,1}\\d{3}-{0,1}\\d{7}$|^\\d{11}$|^\\d{4}-\\d{7}$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    private void showRatingMenu() {
        final Dialog dialog = new Dialog(MapsParent.this);
        dialog.setContentView(R.layout.rating_menu);
        dialog.setTitle("Banjaiga");
        ScaleRatingBar ratingBasedService = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_service);
        ScaleRatingBar ratingBasedCost = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_cost);
        ScaleRatingBar ratingBasedQuality = (ScaleRatingBar) dialog.findViewById(R.id.rating_based_quality);
        dialog.show();


    }

    private void initalizeControls() {

        recyclerView = findViewById(R.id.rv);
        contactInfo = findViewById(R.id.card_contact_form);
        btnRateMe = findViewById(R.id.btn_rate_me);
        cardProfileInfo = findViewById(R.id.rlLayout);
        cardEmailCall = findViewById(R.id.card_email_callform);
        cardupdateloc = findViewById(R.id.card_update_location);
        title = findViewById(R.id.tv_title);
        canceCard = findViewById(R.id.cancel_image);
        cancelArchView = findViewById(R.id.arch_view_cancel);
        cancelupdatefrom = findViewById(R.id.cancel_updateform);
        buttonSend = findViewById(R.id.btnSend);
        buttonSendVendorloc = findViewById(R.id.btn_updateloc);
        ivThumbs = findViewById(R.id.iv_thumbs);
        View view = findViewById(R.id.include_nav);
        ivCall = findViewById(R.id.img_call);
        tvHeading = findViewById(R.id.tvHeading);
        tvOptionDesc = findViewById(R.id.tv_option_desc);
        tvAddressName = findViewById(R.id.tvaddress_name);
        ivMsg = findViewById(R.id.img_msg);
        circleImageView = findViewById(R.id.img_logo);
        ivNearBy = findViewById(R.id.img_near_by);
        ivCardHome = findViewById(R.id.img_card_home);
        ivCardInterior = findViewById(R.id.img_card_interior);
        ivCardArch = findViewById(R.id.img_card_arch);
        ivCardCal = findViewById(R.id.img_card_cal);
        radioGroup = findViewById(R.id.radio_btngroup);


       /* ivHome = findViewById(R.id.img_home);

        ivCurtain = findViewById(R.id.img_curtain);
        ivManGrey = findViewById(R.id.img_mangrey);
        ivBath = findViewById(R.id.img_bath);
        ivKitchen = findViewById(R.id.img_kitchen);
        ivFurniture = findViewById(R.id.img_furniture);
      */

        ivAdminCall = findViewById(R.id.iv_admin_call);
        ivAdminMessage = findViewById(R.id.iv_admin_message);
        emailView = findViewById(R.id.emailViewRb);
        updateloc = findViewById(R.id.thirdView);
        etUsername = findViewById(R.id.et_name);
        etContact = findViewById(R.id.et_contact);
        etVendorName = findViewById(R.id.et_vendorname);
        etVendorContact = findViewById(R.id.et_vendorcontact);
        etvendorLocation = findViewById(R.id.et_vendor_loc);
        etvendorLocation.setFocusable(false);
        archProfileView = findViewById(R.id.profileView);
        footerLayout = findViewById(R.id.footer);
        ivNext = findViewById(R.id.img_next);

        autoCompleteTextViewLoc = findViewById(R.id.search_by_text);
        autoCompleteTextView = findViewById(R.id.search_by_tags);


        ivCall.setOnClickListener(this);
        ivMsg.setOnClickListener(this);

        ivCardHome.setOnClickListener(this);
        ivCardInterior.setOnClickListener(this);
        ivCardArch.setOnClickListener(this);
        ivCardCal.setOnClickListener(this);

        ivNearBy.setOnClickListener(this);
        ivNext.setOnClickListener(this); // top muenu next button click event


    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            isFoundArch = true;
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);
                Double lat = 0.0;
                Double longitude = 0.0;
                autocompletearray.add(dataHelper.arrayArchitecht.get(i).getUserName());
                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();


        // Setting autocompelte keyword suggestions

        Autocompletetextdata();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)
                    //    mMap.animateCamera(CameraUpdateFactory.newLatLng(LOCATIONS_POINTS));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 11.f));
            }
        });

        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getPosition() != null) {
                    AllArchitectsDataModel userData = getUserData(marker.getPosition());
                    if (userData != null) {
                        showMarkerDetail(userData);
                    }
                }
                if (emailView.isChecked()) {
                    contactInfo.setVisibility(View.GONE);
                    cardProfileInfo.setVisibility(View.VISIBLE);

                } else if (archProfileView.isChecked()) {
                    cardProfileInfo.setVisibility(View.GONE);
                    contactInfo.setVisibility(View.VISIBLE);


                }
                if (footerLayout.getVisibility() == View.GONE || footerLayout.getVisibility() == View.VISIBLE) {
                    radioGroup.setVisibility(View.VISIBLE);
                }

                return false;
            }
        });
    }

    private void Autocompletetextdata() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (this, R.layout.select_item, autocompletearray);

        autoCompleteTextView.setThreshold(2);
        autoCompleteTextView.setAdapter(adapter);
    }


    private void showMarkerDetail(AllArchitectsDataModel userData) {
        title.setText("Get banjaiga services");
        phoneNum = userData.getPhoneNumer();
        bitmapString = userData.getLogo();
        tvHeading.setText(userData.getUserName());
        tvAddressName.setText(userData.getArchaddress() + "\n" + userData.getViews() + " Views");
        Usertype = userData.getFlags();
        Log.d("Path", "https://www.banjaiga.com/frontend/web/uploads/" + userData.getFlags() + "s" + "/" + userData.getId() + "/" + userData.getLogo());
        Picasso.with(this).load("https://www.banjaiga.com/frontend/web/uploads/" + userData.getFlags() + "s" + "/" + userData.getId() + "/" + userData.getLogo()).error(R.drawable.app_icon).into(circleImageView);

        // setting intent data on marker click

        archtectactivityintent = new Intent(MapsParent.this, ArchitechtActivity.class);
        archtectactivityintent.putExtra("Vendorimage", "https://www.banjaiga.com/frontend/web/uploads/" + userData.getFlags() + "s" + "/" + userData.getId() + "/" + userData.getLogo());
        archtectactivityintent.putExtra("Name", userData.getUserName());
        archtectactivityintent.putExtra("Phonenumber", userData.getPhoneNumer());
        archtectactivityintent.putExtra("ID", userData.getArchId());
        archtectactivityintent.putExtra("Address", userData.getArchaddress());
        archtectactivityintent.putExtra("UserType", userData.getFlags());


    }

    private AllArchitectsDataModel getUserData(LatLng position) {
        AllArchitectsDataModel allArchitectsDataModel = null;
        if (dataHelper.arrayArchitecht != null && dataHelper.arrayArchitecht.size() > 0) {
            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                String latitude = String.valueOf(position.latitude);
                String longitude = String.valueOf(position.longitude);
                String curentLat = "";
                String curentLong = "";
                if (!dataHelper.arrayArchitecht.get(i).getLatitude().matches("")) {
                    curentLat = dataHelper.arrayArchitecht.get(i).getLatitude();
                }
                if (!dataHelper.arrayArchitecht.get(i).getLongitude().matches("")) {
                    curentLong = dataHelper.arrayArchitecht.get(i).getLongitude();
                }
                if (curentLat.matches(latitude) && curentLong.matches(longitude)) {
                    allArchitectsDataModel = dataHelper.arrayArchitecht.get(i);
                    return allArchitectsDataModel;
                }
            }
        }
        return allArchitectsDataModel;
    }


    private void populaterecyclerviewdata() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        for (int i = 0; i < myImageList.length; i++) {
            Navscrollitemes = new NavScrollitems();
            Navscrollitemes.setDrawable(myImageList[i]);
            Navscrollitemes.setImagetag(imagestag[i]);
            Navscrollitemes.setChangedrawable(myImageListcolor[i]);
            arraynavicons.add(Navscrollitemes);


        }

        adapters_navicons = new Adapters_Nav(MapsParent.this, arraynavicons);
        recyclerView.setAdapter(adapters_navicons);
        adapters_navicons.SetOnItemClickListener(new Adapters_Nav.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // adapters_navicons.changeimage(position);
                ImgTag = arraynavicons.get(position).getImagetag();
                if (ImgTag.equalsIgnoreCase("home")) {

                    ClearSearchview(view); // Clearing searchview hint and keyboard down on every top menu click
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "archlocation");
                }
                if (ImgTag.equalsIgnoreCase("interior")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=108");

                }
                if (ImgTag.equalsIgnoreCase("tiles")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "tiles");


                }
                if (ImgTag.equalsIgnoreCase("kitchen")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=5");

                }
                if (ImgTag.equalsIgnoreCase("furniture")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=15");

                }
                if (ImgTag.equalsIgnoreCase("bath")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=6");
                }

                if (ImgTag.equalsIgnoreCase("lights")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);

                    mVolleyService.getDataVolley("GETCALL", "category&id=148");
                }

            }
        });
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    private void ClearSearchview(View v) {

        // above function is searchview clear hint and for hiding keyboard
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

        autoCompleteTextView.setText("");
        autoCompleteTextViewLoc.setText("");
        autoCompleteTextViewLoc.clearFocus();
        autoCompleteTextView.clearFocus();
        autoCompleteTextViewLoc.setFocusable(false);
        emailView.setChecked(true);
        contactInfo.setVisibility(View.GONE);
        cancelupdatefrom.setVisibility(View.GONE);
        cardProfileInfo.setVisibility(View.GONE);


        // default set value email sending card and  updatelocation card
        clearcardstodefault();


    }

    private void setupMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId() /*to get clicked view id**/) {

            case R.id.img_near_by:
                if (ImgTag.equalsIgnoreCase("home")) {
                    ClearSearchview(view); // Clearing searchview hint and keyboard down on nearby button click
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "archlocation&lat=" + latitude + "&long=" + longitude);
                }
                if (ImgTag.equalsIgnoreCase("interior")) {
                    ClearSearchview(view);
                    mVolleyService.getDataVolley("GETCALL", "category&id=108&lat=" + latitude + "&long=" + longitude);
                }

                if (ImgTag.equalsIgnoreCase("bath")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=6&lat=" + latitude + "&long=" + longitude);
                }
                if (ImgTag.equalsIgnoreCase("kitchen")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=5&lat=" + latitude + "&long=" + longitude);
                }

                if (ImgTag.equalsIgnoreCase("furniture")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=15&lat=" + latitude + "&long=" + longitude);
                }

                if (ImgTag.equalsIgnoreCase("lights")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "category&id=148&lat=" + latitude + "&long=" + longitude);
                }
                if (ImgTag.equalsIgnoreCase("No")) {
                    ClearSearchview(view);
                    Progressloader.loader(MapsParent.this);
                    mVolleyService.getDataVolley("GETCALL", "search&lat=" + latitude + "&long=" + longitude);

                }
                break;

            case R.id.img_call:

                makeCall(phoneNum);
                break;

            case R.id.img_msg:
                if (phonevalidator(phoneNum)) {
                    sendSMS(phoneNum);
                } else {
                    Toast.makeText(MapsParent.this, "Not a valid number to text", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.img_card_home:
                Clearfields(view);
                EMAILTAG = "client";
                tvOptionDesc.setText("I want to build a house");
                cardStateChanger(EMAILTAG);
                break;

            case R.id.img_card_interior:
                Clearfields(view);
                EMAILTAG = "vendor";
                tvOptionDesc.setText("I am a vendor");
                cardStateChanger(EMAILTAG);
                break;

            case R.id.img_card_arch:
                Clearfields(view);
                EMAILTAG = "architect";
                tvOptionDesc.setText("I am an architect");
                cardStateChanger(EMAILTAG);
                break;

            case R.id.img_card_cal:
                Clearfields(view);
                EMAILTAG = "quote";
                tvOptionDesc.setText("I want a free quote");
                cardStateChanger(EMAILTAG);
                break;
            case R.id.img_next:
                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                recyclerView.getLayoutManager().scrollToPosition(llm.findLastVisibleItemPosition() + 1);
                break;
            default:
                break;
        }
    }

    private void Clearfields(View view) {

        autoCompleteTextViewLoc.setText("");
        autoCompleteTextView.setText("");
        autoCompleteTextViewLoc.setFocusable(false);


    }


    private void cardStateChanger(String emaiTag) {
        if (emaiTag.equalsIgnoreCase("client")) {
            ivCardHome.setImageResource(R.drawable.house_selected);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.calculator_round);
        }
        if (emaiTag.equalsIgnoreCase("vendor")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_selected);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("architect")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.architect_selected);
            ivCardCal.setImageResource(R.drawable.calculator_round);

        }
        if (emaiTag.equalsIgnoreCase("quote")) {
            ivCardHome.setImageResource(R.drawable.projects_round);
            ivCardInterior.setImageResource(R.drawable.vendor_round);
            ivCardArch.setImageResource(R.drawable.man_greyround);
            ivCardCal.setImageResource(R.drawable.quote_selected);
        }
    }

    public static class StringClusterItem implements ClusterItem {
        public final String title;
        public final LatLng latLng;
        public final int tag;

        public StringClusterItem(String title, LatLng latLng, int tag) {
            this.title = title;
            this.latLng = latLng;
            this.tag = tag;
        }

        @Override
        public LatLng getPosition() {
            return latLng;
        }
    }


    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {
                //     mSearchviewTag.clearQuery();

                Progressloader.hideloader();
                onResponseFinished(response);
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySucessEmail(String requestType, String response) {
                Toast.makeText(MapsParent.this, response, Toast.LENGTH_SHORT).show();
                Progressloader.hideloader();
                radioGroup.setVisibility(View.GONE);
                etContact.setText("");
                etUsername.setText("");
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
                Progressloader.hideloader();
            }

            @Override
            public void notifyRatingSuccess(String requestType, JSONArray response) {

            }

            @Override
            public void notifyLocationsucess(String requestType, JSONObject response) {
                try {

                    Progressloader.hideloader();
                    Toast.makeText(MapsParent.this, response.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

                Progressloader.hideloader();

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");
            }
        };
    }

    private void onResponseFinished(JSONArray response) {
        try {
            if (dataHelper.arrayArchitecht == null) {
                // mMap.clear();
                dataHelper.arrayArchitecht = new ArrayList<>();
            }
            if (response.length() > 0) {
                dataHelper.arrayArchitecht.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);

                    AllArchitectsDataModel dataModel = new AllArchitectsDataModel(this, jsonObject);
                    dataHelper.arrayArchitecht.add(dataModel);
                }
                plotAddressData();
            } else {
                Toast.makeText(this, "No record found", Toast.LENGTH_SHORT).show();
                mMap.clear();
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    void plotAddressData() {

        //  mClusterManager = new ClusterManager<>(this, mMap);
        mClusterManager.clearItems();
        mMap.setOnCameraChangeListener(mClusterManager);
        if (dataHelper != null && dataHelper.arrayArchitecht.size() > 0) {

            isFoundArch = true;
            mMap.clear();

            for (int i = 0; i < dataHelper.arrayArchitecht.size(); i++) {
                AllArchitectsDataModel arcData = dataHelper.arrayArchitecht.get(i);
                Double lat = 0.0;
                Double longitude = 0.0;
                if (!arcData.getLatitude().matches(""))
                    lat = Double.valueOf(arcData.getLatitude());
                if (!arcData.getLongitude().matches(""))
                    longitude = Double.valueOf(arcData.getLongitude());
                LOCATIONS_POINTS = new LatLng(lat, longitude);
                mClusterManager.addItem(new StringClusterItem("Marker #" + (i + 1), LOCATIONS_POINTS, i));
            }
        } else {
            isFoundArch = false;
        }
        mClusterManager.cluster();
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (isFoundArch)

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LOCATIONS_POINTS, 12f));


            }
        });

        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, mMap, mClusterManager);
        mClusterManager.setRenderer(renderer);


    }


    void changeImageColor(String tag) {
        if (tag.equalsIgnoreCase("home")) {
            ivHome.setImageResource(R.drawable.tiles_yellow);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("interior")) {
            ivCurtain.setImageResource(R.drawable.interio_yellow);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("arch")) {
            ivManGrey.setImageResource(R.drawable.man_yello);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("bath")) {
            ivBath.setImageResource(R.drawable.bath_yellow);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }
        if (tag.equalsIgnoreCase("kitchen")) {
            ivKitchen.setImageResource(R.drawable.kitchen_yellow);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivFurniture.setImageResource(R.drawable.furniture_grey);
        }


        if (tag.equalsIgnoreCase("furniture")) {
            ivKitchen.setImageResource(R.drawable.img_kitchen);
            ivBath.setImageResource(R.drawable.bath_grey);
            ivHome.setImageResource(R.drawable.tiles_grey);
            ivCurtain.setImageResource(R.drawable.curtains_grey);
            ivManGrey.setImageResource(R.drawable.man_grey);
            ivFurniture.setImageResource(R.drawable.furniture_yellow);
        }
    }

    void changelatlongtoaddress(double latitude, double longitude) {
        List<Address> addresses;
        Geocoder geoCoder = new Geocoder(MapsParent.this);
        try {

            addresses = geoCoder.getFromLocation(latitude, longitude, 1);
            etvendorLocation.setText(addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality());
        } catch (IOException e) { // TODO Auto-generated catch block
            e.printStackTrace();
        }

       /* pos = new LatLng(latitude, longtitude);
        googleMap.addMarker(new MarkerOptions().icon(
                BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(pos));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);*/
    }

    public void sendSMS(String phone_num) {
        // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }

    private void makeCall(String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                autoCompleteTextViewLoc.setText(String.valueOf(place.getName()));
                autoCompleteTextViewLoc.setSelection(autoCompleteTextViewLoc.getText().length());
                autoCompleteTextViewLoc.setFocusableInTouchMode(true);
                autoCompleteTextViewLoc.requestFocus();
                LatLng destination = place.getLatLng();
                searchLatitude = destination.latitude;
                searchLongitude = destination.longitude;
                Showhidekeyboard = "no";

            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE1) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                etvendorLocation.setText(place.getName());
                LatLng destination = place.getLatLng();
                vendorLatitude = destination.latitude;
                vendorLongitude = destination.longitude;

            }
        }
    }

    @Override
    public void onResume() {
        if (Showhidekeyboard.equalsIgnoreCase("yes")) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        } else {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        }
        super.onResume();
    }

    @Override
    public void onStart() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onStart();
    }

}
