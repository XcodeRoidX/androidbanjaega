package com.design.hrh.banjaegaandroidapp.AppController;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.IResult;
import com.design.hrh.banjaegaandroidapp.ExternalFiles.Request.VolleyService;
import com.design.hrh.banjaegaandroidapp.Helper.MyHelper;
import com.design.hrh.banjaegaandroidapp.Model.SocialUserModel;
import com.design.hrh.banjaegaandroidapp.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArchitechtActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {
    LoginButton loginfButton;
    CallbackManager callbackManager;
    MyHelper myHelper;
    TextView txtUserName;
    LinearLayout socialLayout, ratingMenu;
    RecyclerView recyclerView;
    CircleImageView circleImageView;
    TextView tvHeading, tvAddressName, tvReview, tvCategory;
    ImageView ivCall, ivMsg;
    ScaleRatingBar Ratingbasedoverall, Ratingbasedquality, Ratingbasedpricing, Rating_based_service, ratingInputData;
    String phoneNum, userid, userType;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    private static final String TAG = ArchitechtActivity.class.getSimpleName();
    ImageView buttonNext, buttonBack;
    int count = 0;
    float ratingPrice;
    float ratingQuality;
    float ratingService;
    Button buttonSubmit;
    GoogleApiClient mGoogleApiClient;
    Context con;
    SignInButton btnSignIn;
    private static final int RC_SIGN_IN = 007;
    LinearLayout ivDone, reviewsLayout;
    boolean socialLogin = false;
    EditText etSubmitReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_architecht);
        con = this;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(con)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage((ArchitechtActivity) con/* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addOnConnectionFailedListener(this)
                .addScope(new Scope(Scopes.PROFILE))
                .build();

        myHelper = MyHelper.getInstance(ArchitechtActivity.this);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        // Customizing G+ button
        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        loginfButton = (LoginButton) findViewById(R.id.login_button);
        initVolleyCallback();
        mVolleyService = new VolleyService(mResultCallback, this);

        setUpFaceBook();
        init();
        getvendordata();
        OnClickCalls();
        VolleyCallRatings();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                startActivity(new Intent(ArchitechtActivity.this, MapsParent.class));
                                finish();
                            }
                        }
                    });
        }
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    private void VolleyCallRatings() {
        mVolleyService.getvendorRating("GETCALL", "getrating&userID=" + userid);
    }

    private void OnClickCalls() {
        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(phoneNum);
            }
        });

        ivMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS(phoneNum);
            }
        });
        if (count == 0) {
            buttonBack.setVisibility(View.INVISIBLE);
            buttonNext.setVisibility(View.INVISIBLE);
            socialLayout.setVisibility(View.VISIBLE);
        }

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    manageRatingView();
                } else if (count > 0 && count < 6) {
                    count = count - 1;
                    manageRatingView();
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 1) {
                    saveVendorRating();
                    count = count + 1;
                    manageRatingView();
                } else if (count > 1 && count < 6) {
                    saveVendorRating();
                    count = count + 1;
                    manageRatingView();
                }
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SocialUserModel socialUserModel = myHelper.socialUserModels.get(0);
                    String username = null;
                    try {
                        username = URLEncoder.encode(socialUserModel.getUserName(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String review = URLEncoder.encode(etSubmitReview.getText().toString());
                    String param = "saveUserRating" + "/" + "banjaiga/" + userid + "/" + username + "/"
                            + socialUserModel.getUserEmail() + "/" + userType + "/" + socialUserModel.getImageUrl() + "/" + ratingService + "/"
                            + ratingPrice + "/" + ratingQuality + "/" + review;

                    mVolleyService.saveUserRatings("saveUserRating/" + param, "GETCALL");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getvendordata() {
        Intent intent = getIntent();
        Picasso.with(this).load(intent.getStringExtra("Vendorimage")).error(R.drawable.app_icon).into(circleImageView);
        tvHeading.setText(intent.getStringExtra("Name"));
        tvAddressName.setText(intent.getStringExtra("Address"));
        phoneNum = intent.getStringExtra("Phonenumber");
        userid = intent.getStringExtra("ID");
        userType = intent.getStringExtra("UserType");
    }

    private void init() {
        txtUserName = (TextView) findViewById(R.id.txt_User_Name);
        etSubmitReview = (EditText) findViewById(R.id.et_submit_review);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        tvAddressName = (TextView) findViewById(R.id.tvaddress_name);
        tvReview = (TextView) findViewById(R.id.review);
        tvCategory = (TextView) findViewById(R.id.category);
        buttonNext = (ImageView) findViewById(R.id.btn_next);
        buttonBack = (ImageView) findViewById(R.id.btn_back);
        buttonSubmit = (Button) findViewById(R.id.btn_submit);
        ivDone = (LinearLayout) findViewById(R.id.lv_done);
        reviewsLayout = (LinearLayout) findViewById(R.id.reviews_layout);
        ivDone.setVisibility(View.GONE);
        Ratingbasedoverall = (ScaleRatingBar) findViewById(R.id.rating_based_overall);
        Rating_based_service = (ScaleRatingBar) findViewById(R.id.rating_based_service);
        Ratingbasedpricing = (ScaleRatingBar) findViewById(R.id.rating_based_pricing);
        Ratingbasedquality = (ScaleRatingBar) findViewById(R.id.rating_based_quality);
        ratingInputData = (ScaleRatingBar) findViewById(R.id.rating_input_data);

        Ratingbasedoverall.setIsIndicator(true);
        Ratingbasedquality.setIsIndicator(true);
        Rating_based_service.setIsIndicator(true);
        Ratingbasedpricing.setIsIndicator(true);

        circleImageView = (CircleImageView) findViewById(R.id.img_logo);
        ivCall = (ImageView) findViewById(R.id.img_call);
        ivMsg = (ImageView) findViewById(R.id.img_msg);

        socialLayout = (LinearLayout) findViewById(R.id.social_lay);
        ratingMenu = (LinearLayout) findViewById(R.id.rating_menu);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //recyclerView = (RecyclerView) findV iewById(R.id.rv_layout);
        //recyclerView.setLayoutManager(layoutManager);

    }


    private void setUpFaceBook() {
        loginfButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginfButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                count = 1;
                socialLayout.setVisibility(View.GONE);
                manageRatingView();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    socialLogin = true;
                                    myHelper.socialUserModels = new ArrayList<>();
                                    SocialUserModel socialUserModel = new SocialUserModel(object, "FB");
                                    myHelper.socialUserModels.add(socialUserModel);
                                    txtUserName.setVisibility(View.VISIBLE);
                                    txtUserName.setText("Welcome " + myHelper.socialUserModels.get(0).getUserName());
                                    btnSignIn.setVisibility(View.GONE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }// 01/31/1980 format
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                try {
                    Log.v("LoginActivity", exception.getCause().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            loginfButton.setVisibility(View.GONE);
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());
            myHelper.socialUserModels = new ArrayList<>();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", acct.getId());
                jsonObject.put("name", acct.getDisplayName());
                jsonObject.put("email", acct.getEmail());
                jsonObject.put("imageUrl", acct.getPhotoUrl().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            SocialUserModel socialUserModel = new SocialUserModel(jsonObject, "Google");
            myHelper.socialUserModels.add(socialUserModel);
            txtUserName.setVisibility(View.VISIBLE);
            txtUserName.setText("Welcome " + myHelper.socialUserModels.get(0).getUserName());
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            count = 1;
            socialLayout.setVisibility(View.GONE);
            manageRatingView();
            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    private void makeCall(String phone_num) {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    public void sendSMS(String phone_num) {
        // The number on which you want to send SMS
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phone_num, null)));
    }


    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
                onSuccess();
            }

            @Override
            public void notifySuccess(String requestType, JSONArray response) {
                //     mSearchviewTag.clearQuery();

                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + response);
            }

            @Override
            public void notifySucessEmail(String requestType, String response) {


            }

            @Override
            public void notifyRatingSuccess(String requestType, JSONArray response) {
                onResponseFinished(response);
            }

            @Override
            public void notifyLocationsucess(String requestType, JSONObject response) {

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                Log.d(TAG, "Volley requester " + requestType);
                Log.d(TAG, "Volley JSON post" + "That didn't work!");
            }
        };
    }


    private void onResponseFinished(JSONArray response) {
        try {
            float total = 0;
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);

               /* Ratingbasedquality.setRating(Float.parseFloat(response.getString("quality")));
                Ratingbasedpricing.setRating(Float.parseFloat(response.getString("pricing")));
                Rating_based_service.setRating(Float.parseFloat(response.getString("service")));
               */
                if (jsonObject.has("service"))
                    Rating_based_service.setRating(Float.parseFloat(jsonObject.getString("service")));
                else if (jsonObject.has("quality"))
                    Ratingbasedoverall.setRating(Float.parseFloat(jsonObject.getString("quality")));
                else if (jsonObject.has("price"))
                    Ratingbasedpricing.setRating(Float.parseFloat(jsonObject.getString("price")));


                if (Ratingbasedquality != null)
                    total += Ratingbasedquality.getRating();
                else if (Ratingbasedpricing != null)
                    total += Ratingbasedpricing.getRating();
                else if (Rating_based_service != null)
                    total += Rating_based_service.getRating();

                float average = total / 3;
                if (Ratingbasedoverall != null)
                    Ratingbasedoverall.setRating(average);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveVendorRating() {
        if (count == 1) {
            ratingQuality = ratingInputData.getRating();
        } else if (count == 2) {
            ratingPrice = ratingInputData.getRating();
        } else if (count == 3) {
            ratingService = ratingInputData.getRating();
        }
    }

    private void manageRatingView() {
        if (count == 0) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            buttonNext.setVisibility(View.INVISIBLE);
            buttonBack.setVisibility(View.INVISIBLE);
            socialLayout.setVisibility(View.VISIBLE);
            ratingMenu.setVisibility(View.GONE);
        } else if (count == 1) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            ratingMenu.setVisibility(View.VISIBLE);
            tvCategory.setText("Quality");
        } else if (count == 2) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            tvCategory.setText("Price");
        } else if (count == 3) {
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.GONE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
            buttonBack.setVisibility(View.VISIBLE);
            tvCategory.setText("Service");
        } else if (count == 4) {
            etSubmitReview.setVisibility(View.VISIBLE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.GONE);
        } else if(count == 5){
            etSubmitReview.setVisibility(View.GONE);
            buttonSubmit.setVisibility(View.VISIBLE);
            socialLayout.setVisibility(View.GONE);
            ratingMenu.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        signIn();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void onSuccess() {
        Toast.makeText(con, "Review Submitted Successfully", Toast.LENGTH_SHORT).show();
        logoutSocial();
    }

    private void logoutSocial() {
            LoginManager.getInstance().logOut();
            startActivity(new Intent(ArchitechtActivity.this, MapsParent.class));
            finish();
    }
}
//https://www.banjaiga.com/web_services/index.php/saveUserRating/banjaiga/5141224/aliraza/raza@banjaiga.com/architect/https://www.banjaiga.com/themes/banjaiga/img/aliRazaH.jpg/5/5/5/very%20good%20services
///